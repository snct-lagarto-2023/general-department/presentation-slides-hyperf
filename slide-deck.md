---
marp: true
theme: academic
paginate: true
math: katex
---

<!-- _class: lead -->

# Introdução ao Hyperf

#### Um guia básico para iniciantes

**Aluno: Reinan Gabriel Dos Santos Souza**
Bacharelado em Sistemas de Informação
Instituto Federal de Sergipe
Campus Lagarto

<!-- _footer: '25 de outubro de 2023' -->

---

![bg left:40% 80%](https://avatars.githubusercontent.com/u/28494067?v=4)

**Reinan Gabriel Dos Santos Souza**

Sou um entusiasta de tecnologia apaixonado por aprender e explorar novas áreas.

Desenvolvedor desde 2018
Engenheiro DevOps Pleno – **MOVA**

https://linktr.ee/reinanhs

![w:128 h:128](https://www.qrtag.net/api/qr_1280.png?url=https://linktr.ee/reinanhs)

---

![bg left:40% 80%](https://www.qrtag.net/api/qr_1280.png?url=https://gitlab.com/snct-lagarto-2023)

**Arquivos para a apresentação**

Todos os materiais relacionados à apresentação do minicurso estão disponíveis digitalmente no meu repositório do **Gitlab**. Para acessar esses recursos, basta escanear o **QR Code** na imagem ao lado.

> Gitlab: https://gitlab.com/snct-lagarto-2023
> Github: https://github.com/ReinanHS

---

<!-- _header: Introdução -->

Nesta apresentação, exploraremos o PHP Hyperf, um framework de alto desempenho para aplicações web.

Vamos descobrir como essa tecnologia pode impulsionar:

- Performance dos nossos projetos.
- Otimizando o tempo de resposta.
- Otimizando o escalabilidade.

---

<!-- _header: Contexto Histórico -->

- Durante muito tempo, a linguagem de programação PHP foi uma escolha popular para o desenvolvimento de aplicações web.
- Sua popularidade se deve a vários fatores, incluindo:
  - Fácil aprendizagem.
  - Ampla documentação.
  - Vasta comunidade de desenvolvedores.
  - Ampla variedade de frameworks disponíveis

---

o PHP tinha um modelo de execução síncrona, que facilitava a escrita de código para aplicações web mais simples e com requisitos de processamento menos intensivos. Além disso, existiam ferramentas que facilitavam a sua utilização:

- WordPress
- Joomla
- Drupal
- Magento

---

Exemplo:

![center](./imagens/exemplo-sispublic.png)

---

No entanto, com a **evolução da tecnologia** e o aumento das demandas de desempenho e escalabilidade, o mercado está mudando para aplicações que trabalham de forma assíncrona.

---

A **programação assíncrona** permite que várias operações de entrada e saída sejam executadas simultaneamente, sem bloquear a execução do programa.

![center](./imagens/asynchronous-programming-example.webp)

> Fonte: https://coldbox.ortusbooks.com/digging-deeper/promises-async-programming

---

**Caso de estudo fictício**: Loja que atende os moradores da cidade de Boquim

![w:800 center](./imagens/exemplo-site.png)

---

**Caso de estudo fictício**: A loja vende para todos do estado de Sergipe

![w:800 center](./imagens/ze-feliz-com-site.png)

---

**Caso de estudo fictício**: A loja vende para todo o Brasil
O site dele não aguentou o tráfego. 

![w:800 center](./imagens/ze-bad-com-site.png)

---

A transição para aplicações assíncronas é impulsionada por várias razões:

- O aumento do tráfego na web.
- O crescimento do número de usuários.

A programação assíncrona permite que as aplicações gerenciem eficientemente essas requisições, melhorando o desempenho e a escalabilidade.

---

As aplicações modernas frequentemente dependem de operações intensivas de entrada e saída:

- Chamadas a APIs externas.
- Acesso a bancos de dados.
- Integração com serviços de terceiros.

A programação assíncrona permite que essas operações sejam executadas de forma não bloqueante.

---

A linguagem de programação PHP foi amplamente utilizada no passado devido:

- Sua facilidade de uso e ampla variedade de frameworks disponíveis.
- Ao modelo de execução síncrona que se adequava bem a muitos casos de uso. 

No entanto, com o aumento das demandas de desempenho, escalabilidade e interatividade em tempo real, o mercado está migrando para aplicações assíncronas.

---

<!-- _header: Como surgiu o Hyperf? -->

O framework **Hyperf** em 2018, destinado a trazer soluções para a programação assíncrona utilizando a linguagem de programação PHP.

Ao contrário de muitos outros frameworks disponíveis no mercado para PHP, o **Hyperf** foi concebido desde o início para se adaptar aos novos padrões exigidos pelo mercado para aplicações modernas.

---

Agora que temos uma compreensão do contexto que levou à criação do Hyperf, vamos explorar como o framework incorporou programação assíncrona em seu código.

- Como ele superou as limitações do PHP?

---

<!-- _header: O que é o Swoole? -->

O Swoole é uma biblioteca para rede assíncrona em PHP que proporciona um ambiente de execução de alto desempenho e suporte para programação assíncrona.

Diferentemente do modelo de execução síncrona tradicional do PHP, o Swoole permite que os desenvolvedores escrevam código assíncrono e aproveitem as vantagens da programação não bloqueante.

---

Exemplo:

```php
<?php

use function Swoole\Coroutine\run;
use function Swoole\Coroutine\go;

run(function() {
    go(function(){
        sleep(1);
        echo "Execução do meu primeiro processo";
    });

    go(function(){
        echo "Execução do meu segundo processo.";
    });
});
```

---

<!-- _header: O que é o Hyperf? -->

O Hyperf é um framework PHP de alto desempenho para o desenvolvimento de aplicações web, baseado no ecossistema Swoole.

Inspirado pelo **Laravel** e outros frameworks populares, o Hyperf foi desenvolvido com o objetivo de fornecer uma solução eficiente e escalável para a criação de aplicações modernas.

---

o Hyperf foi projetado desde o início para se destacar em:

- Performance
- Microsserviços
- Componentes desacoplados

---

Diferentemente de muitos outros frameworks no mercado, como:

- Laravel
- Symfony
- Slim
- CakePHP

o Hyperf não foi adaptado para utilizar o Swoole, mas sim concebido para trabalhar em conjunto com ele. 

Todos os seus componentes foram desenvolvidos considerando o uso do Swoole, proporcionando uma integração harmoniosa entre as tecnologias.

---

<!-- _header: Benchmark -->

Temos discutido a questão da performance e as distinções entre o Swoole e o PHP convencional. Agora, é hora de analisarmos os dados concretos!

Eu compilei alguns resultados de testes realizados em alguns dos principais frameworks em PHP. É importante salientar que esses testes não foram conduzidos por mim, mas foram extraídos de uma publicação específica:

- [Hyperf - PHP Coroutine Framework baseado em Swoole](https://leocarmo.dev/hyperf-php-coroutine-framework-baseado-em-swoole)


> Benchmark feito pelo **Leonardo do Carmo**: https://hashnode.com/@leocarmo

---

Laravel 8 com PHP 8.0 (93 rq/s)

```shell
Running 10s test @ http://0.0.0.0:8000
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   550.01ms  335.24ms   1.12s    63.01%
    Req/Sec    12.38      8.80    70.00     84.72%
  938 requests in 10.10s, 16.64MB read
  Socket errors: connect 155, read 1214, write 12, timeout 0
Requests/sec:     92.83
Transfer/sec:      1.65MB
```

---

Lumen 8 com PHP 8.0 (279 rq/s)

```shell
Running 10s test @ http://0.0.0.0:8000
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   108.93ms   41.13ms 193.70ms   76.31%
    Req/Sec    73.96     40.36   184.00     69.49%
  2820 requests in 10.10s, 751.82KB read
  Socket errors: connect 157, read 3987, write 4, timeout 0
Requests/sec:    279.10
Transfer/sec:     74.41KB
```

---

Laravel Octane com PHP 8.0 e Swoole (1.273 rq/s)

```shell
Running 10s test @ http://0.0.0.0:8000
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   110.06ms  190.13ms   1.65s    84.78%
    Req/Sec   118.57     96.64   550.00     69.97%
  12796 requests in 10.05s, 13.09MB read
  Socket errors: connect 157, read 101, write 4, timeout 0
Requests/sec:   1273.75
Transfer/sec:      1.30MB
```

---

Hyperf 2.2 com PHP 8.0 (95.515 rq/s)

```shell
Running 10s test @ http://0.0.0.0:9502
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.55ms    1.37ms  21.38ms   90.08%
    Req/Sec    12.02k     1.77k   15.27k    58.88%
  956407 requests in 10.01s, 173.30MB read
  Socket errors: connect 779, read 95, write 0, timeout 0
Requests/sec:  95515.68
Transfer/sec:     17.31MB
```

---

# Comparações

| Framework    |  rq/s  |
|--------------|--------|
|Laravel       | 92     |
|Lumen         | 279    |
|Laravel Octane| 1.273  |
|Hyperf        | 95.515 |

o Hyperf é mais de 986 vezes mais rápido do que o Laravel

> Fonte: https://leocarmo.dev/hyperf-php-coroutine-framework-baseado-em-swoole

---

<!-- _header: Principais recursos do Hyperf -->

Veja uma lista com as principais funcionalidades que esse framework oferece:

1. Alta performance
2. Arquitetura baseada em componentes
3. Suporte nativo para programação assíncrona
4. Ecossistema extensível
5. Suporte para contêineres de injeção de dependência

---

<!-- _header: Principais diferenciais do Hyperf em relação a outros frameworks PHP populares -->

O Hyperf se diferencia de outros frameworks PHP devido:

- Suporte nativo à programação assíncrona.
- Foco em desempenho e escalabilidade.
- Arquitetura orientada a microsserviços.
- Ecossistema específico e flexibilidade personalizável.

Essas características tornam o Hyperf uma escolha atraente para o desenvolvimento de aplicações web que exigem alto desempenho e lidam com um grande volume de requisições assíncronas.

---

<!-- _header: Desafios e considerações -->

Embora o PHP Hyperf seja uma excelente escolha para aplicações de alta performance, é importante estar ciente dos desafios e considerações envolvidos. Alguns pontos a serem considerados incluem:

- Curva de aprendizado inicial
- Compatibilidade com bibliotecas existentes
- Manutenção e suporte da comunidade

---

<!-- _header: Quais empresas utilizam esse framework? -->

Atualmente, várias empresas adotam o uso do Hyperf em suas aplicações:

- PicPay
- MOVA SEP
- Linksoft
- MangaToon

> Fonte: https://github.com/hyperf/hyperf/issues/1400

---

<!-- _header: Vagas para Hyperf -->

![center](./imagens/vaga-hyperf.png)

> Fonte: https://picpay.com/oportunidades-de-emprego-e-carreiras/central-de-vagas/4302190005

---

<!-- _header: A comunidade do Hyperf -->

A ativa participação da comunidade é evidenciada pela constante evolução do projeto, que se beneficia de:

- Contribuições regulares
- Correções de bugs
- Melhorias contínuas

A comunidade no GitHub do Hyperf oferece suporte e recursos valiosos para os desenvolvedores que desejam aproveitar ao máximo o potencial do framework.

> Github: https://github.com/hyperf/hyperf

---

![center w:900](./imagens/issue-exemplo-01.png)

> Fonte: https://github.com/hyperf/hyperf/issues/6144

---

![center w:900](./imagens/issue-exemplo-02.png)

> Fonte: https://github.com/hyperf/hyperf/issues/5871

---

Minha primeira contribuição a nível de código ao projeto Hyperf

![center w:800](./imagens/merge-exemplo-03.png)

> Fonte: https://github.com/hyperf/hyperf/pull/5131

---

Comunidades no GitHub relacionados ao Hyperf

![center w:900](./imagens/opencodeco.png)

> Fonte: https://github.com/opencodeco

---

Comunidades no GitHub relacionados ao Hyperf

![center w:900](./imagens/friendsofhyperf.png)

> Fonte: https://github.com/friendsofhyperf

---

Comunidade Brasileira no Discord

![center w:800](./imagens/hyperf-discord.png)

> Fonte: https://discord.gg/sqpEU2cV

---

<!-- _header: Apresentação prática sobre o Hyperf -->

Para começar, vamos acessar o repositório no **Gitlab** listado abaixo.

![w:328 h:328 center](https://www.qrtag.net/api/qr_1280.png?url=https://gitlab.com/snct-lagarto-2023/development-department/backend/poc-project-cloud-run-hyperf)

> Fonte: https://gitlab.com/snct-lagarto-2023/development-department/backend/poc-project-cloud-run-hyperf

---

<!-- _header: Conclusão -->

Chegamos ao fim desta apresentação do mini curso sobre Hyperf. 

- Espero que vocês tenham compreendido a importância desse framework para impulsionar projetos web. 
- Com sua *arquitetura sólida*, *recursos avançados* e *casos de sucesso comprovados*, o Hyperf se destaca como uma escolha poderosa para quem busca máxima performance. 