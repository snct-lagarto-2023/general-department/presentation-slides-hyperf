# Introdução ao Hyperf

Este repositório tem a finalidade de armazenar os materiais da apresentação que abordará o tema "*Introdução ao Hyperf: Um guia básico para iniciantes*".

## Software stack

Esse projeto roda nos seguintes softwares:

- Marp 2.7.0
- Docker

## Changelog

Consulte [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.

## Contributors

Sinta-se à vontade para contribuir com melhorias, correções de bugs ou adicionar recursos a este repositório. Basta criar um fork do projeto, fazer suas alterações e enviar uma solicitação pull. Suas contribuições são bem-vindas!

Quer fazer parte deste projeto? leia [como contribuir](CONTRIBUTING.md).

## License

Este projeto está licenciado sob a licença MIT. Consulte o arquivo [LICENSE](LICENSE) para obter mais detalhes.

## Referências bibliográficas

Esta publicação foi embasada em algumas referências bibliográficas, que foram fundamentais para a construção do conteúdo apresentado. Recomendamos a leitura dessas referências, a fim de obter uma compreensão mais aprofundada do tema abordado neste artigo. Ao consultar essas fontes, você terá acesso a informações adicionais e poderá explorar aspectos específicos do Hyperf com mais detalhes.

- Documentação oficial do Hyperf: Disponível em https://hyperf.wiki/
- Repositório oficial do Hyperf no GitHub: Disponível em https://github.com/hyperf/hyperf
- Swoole Documentation: Disponível em https://www.swoole.co.uk/docs/
- Observabilidade com Hyperf e OpenTelemetry: Disponível em https://medium.com/inside-picpay/observabilidade-com-hyperf-e-opentelemetry-dd698844eda
- Guia completo do framework Hyperf para construção de aplicativos PHP modernos: Disponível em https://programadoresdepre.com.br/guia-completo-do-framework-hyperf-para-construcao-de-aplicativos-php-modernos/
- Why Asynchronous PHP is the Future: Disponível em https://nordikcoin.com/blog/asynchronous-php-future/
- Hyperf - PHP Coroutine Framework baseado em Swoole: Disponível em https://leocarmo.dev/hyperf-php-coroutine-framework-baseado-em-swoole